package org.cinema.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.cinema.entidades.jpa.Filme;
import org.cinema.utils.MessagesHelper;
import org.cinema.utils.UrlDeImagensHelper;
import org.cinema.services.FilmeService;

@Named
@ViewScoped
public class ListagemDeFilmesBean implements Serializable {

    private static final long serialVersionUID = -7541082218215581325L;

    
    @Inject
    private transient MessagesHelper messagesHelper;
    
    
    @Inject
    private transient FilmeService service;
    
    
    @Inject
    private transient UrlDeImagensHelper urlDeImagensHelper;
    
    
    public List<Filme> getAll() {
        return service.list();
    }

    public String getUrlDaImagemDoFilme(Long filmeId) {
        return urlDeImagensHelper.getUrlDaImagemDoFilme(filmeId, "cartaz");
    }

}
