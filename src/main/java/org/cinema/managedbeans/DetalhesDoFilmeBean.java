package org.cinema.managedbeans;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.cinema.entidades.jpa.Filme;
import org.cinema.utils.MessagesHelper;
import org.cinema.utils.UrlDeImagensHelper;
import org.cinema.services.FilmeService;

// bean da pagina de detalhes do filme

@Named
@ViewScoped
public class DetalhesDoFilmeBean implements Serializable {

    private static final long serialVersionUID = -7541082218215581325L;

    @Inject
    private transient MessagesHelper messagesHelper;

    @Inject
    private transient FilmeService service;

    @Inject
    private transient UrlDeImagensHelper urlDeImagensHelper;

    private Filme filme;
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void carregar() {
        filme = service.buscar(id);
    }

    public Filme getFilme() {
        return filme;
    }

    public void setFilme(Filme filme) {
        this.filme = filme;
    }

    public String getUrlDaImagemDoFilme() {
        if (filme == null || filme.getId() == null) {
            return "";
        }

        return urlDeImagensHelper
                .getUrlDaImagemDoFilme(filme.getId(), "cartaz");
    }

}
