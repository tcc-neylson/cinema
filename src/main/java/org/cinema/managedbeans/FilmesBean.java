package org.cinema.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.cinema.entidades.jpa.Filme;
import org.cinema.entidades.jpa.Filme.Censura;
import org.cinema.entidades.sistema.Imagem;
import org.cinema.utils.MessagesHelper;
import org.cinema.infra.imagens.RepositorioDeImagensDeFilmes;
import org.cinema.infra.imagens.RepositorioDeImagensTemporarias;
import org.cinema.utils.UrlDeImagensHelper;
import org.cinema.services.FilmeService;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

@Named
@ViewScoped
public class FilmesBean implements Serializable {

    private static final long serialVersionUID = -7541082218215581325L;

    @Inject
    private transient MessagesHelper messagesHelper;
    
    @Inject
    private transient FilmeService service;
    
    @Inject
    private transient RepositorioDeImagensTemporarias repoDeImagensTemporarias;
    
    @Inject
    private transient RepositorioDeImagensDeFilmes repoDeImagensDeFilmes;
    
    @Inject
    private transient UrlDeImagensHelper urlDeImagensHelper;

    private Filme filme = new Filme();
    private boolean editar = false;

    private UUID idDaImagemTemporaria;

    
    public void limpar() {
        this.filme = new Filme();
        setEditar(false);
        limparImagemTemporaria();
    }

    // carrega um filme para ediçao
    public void carregar(Long id) {
        filme = service.buscar(id);
        setEditar(true);
        limparImagemTemporaria();
    }

    // o metodo do bean e executado em uma transacao, necessario nos beans gerenciados pelo CDI
    @Transactional
    public void salvar() throws IOException {
        
        // se a imagem nao foi selecionada envia uma mensagem de erro
        if (idDaImagemTemporaria == null) {
            FacesContext.getCurrentInstance().validationFailed();
            messagesHelper.addMessage(new FacesMessage(FacesMessage.SEVERITY_WARN, "Por favor, selecione uma imagem de cartaz", ""));
            
            return;
        }
        
        // salva o filme
        service.persist(filme);
        
        // salva a imagem
        Imagem imagem = repoDeImagensTemporarias.obterImagemCom(idDaImagemTemporaria);
        repoDeImagensDeFilmes.salvar(imagem, filme.getId(), "cartaz");

        // envia uma mensagem de sucesso
        messagesHelper.addMessage(new FacesMessage("Filme cadastrado com sucesso!"));
    }

    // o metodo do bean e executado em uma transacao, necessario nos beans gerenciados pelo CDI
    @Transactional
    public void atualizar() throws IOException {
        // atualiza o filme
        service.merge(filme);
        
        // atualiza a imagem somente se ela foi enviada
        if (idDaImagemTemporaria != null) {
            Imagem imagem = repoDeImagensTemporarias.obterImagemCom(idDaImagemTemporaria);
            repoDeImagensDeFilmes.salvar(imagem, filme.getId(), "cartaz");
        }
        
        // envia uma mensagem de sucesso
        messagesHelper.addMessage(new FacesMessage("Filme atualizado com sucesso!"));
    }

    // o metodo do bean e executado em uma transacao, necessario nos beans gerenciados pelo CDI
    @Transactional
    public void remover(Long id) {
        filme = service.buscar(id); // busca o filme
        service.remover(filme); // remove o filme
        
        // envia uma mensage de sucesso
        messagesHelper.addMessage(new FacesMessage("Filme removido com sucesso!"));
    }

    public List<Filme> getAll() {
        return service.list();
    }

    public Filme getFilme() {
        return filme;
    }

    public void setFilme(Filme filme) {
        this.filme = filme;
    }

    public boolean isEditar() {
        return editar;
    }

    public void setEditar(boolean editar) {
        this.editar = editar;
    }

    // manipula o upload de imagems
    public void handleFileUpload(FileUploadEvent event) throws IOException {
        
        // obtenho a imagem enviada
        UploadedFile uploadedFile = event.getFile();
        
        // crio uma instance de Imagem
        Imagem imagem = new Imagem(uploadedFile);
        
        // limpo a imagem temporaria anterior
        limparImagemTemporaria();
        
        // salvo a imagem temporaria e atribuo o id para a variavel do bean
        idDaImagemTemporaria = repoDeImagensTemporarias.salvar(imagem);
    }

    public UUID getIdImagemTemporaria() {
        return idDaImagemTemporaria;
    }

    // serve para limpar a imagem temporaria atual
    private void limparImagemTemporaria() {
        if (idDaImagemTemporaria != null) {
            repoDeImagensTemporarias.removerImagemCom(idDaImagemTemporaria);
            idDaImagemTemporaria = null;
        }
    }

    // retorna o endereco da imagem para ser visualizada na pagina
    public String getUrlDaImagemTemporaria() {

        if (idDaImagemTemporaria != null) {
            return urlDeImagensHelper.getUrlDeImagemTemporaria(idDaImagemTemporaria.toString());
        }

        if (editar == true) {
            return getUrlDaImagemDoFilme();
        }

        // imagem default
        return "/resources/img/default-cartaz.png";
    }

    public String getUrlDaImagemDoFilme() {
        if (filme == null || filme.getId() == null) {
            return "";
        }

        return urlDeImagensHelper
                .getUrlDaImagemDoFilme(filme.getId(), "cartaz");
    }

    public String getTituloDoModalDoFormulario() {
        if (editar) {
            return "Editar filme";
        }

        return "Cadastrar filme";
    }

    public SelectItem[] getCensuras() {

        List<SelectItem> itens = new ArrayList<>();

        for (Censura censura : Censura.values()) {
            itens.add(new SelectItem(censura, censura.toString()));
        }

        return itens.toArray(new SelectItem[itens.size()]);
    }

    public List<String> completarGenero(String query) {

        List<String> generos = Arrays.asList("Ação", "Animação", "Aventura",
                "Comédia", "Documentário", "Drama", "Família", "Fantasia",
                "Ficção científica", "Filme noir", "Guerra", "Musical",
                "Policial", "Romance", "Suspense", "Terror", "Thriller",
                "Western");

        List<String> generosFiltrados = generos.stream().filter((genero) -> {
            return genero.toLowerCase().contains(query.toLowerCase());
        }).collect(Collectors.toList());

        generosFiltrados.add(0, query);

        return generosFiltrados;
    }
}
