/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cinema.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.cinema.session.SessaoDeAutenticacao;
import org.cinema.utils.MessagesHelper;


// bean para autenticar o usuario

@Named
@ViewScoped
public class AutenticacaoBean implements Serializable {
    
    private String senha;

    // injeta um objeto MessagesHelper
    @Inject
    private transient MessagesHelper messagesHelper;
    
    // injeta o objeto da sessao de autenticacao
    @Inject
    private transient SessaoDeAutenticacao sessao;
    
    
    public void autenticar() throws IOException {
        
        String senhaDoAdministrador = "123456";
            
        // se a senha e igual a do admnistrador
        if(senha.equals(senhaDoAdministrador)) {
            
            // define o usuario como autenticado
            sessao.setAdministradorEstaAutenticado(true);
            
            // redirediona para a pagina de cadastrar filmes
            FacesContext.getCurrentInstance().getExternalContext().redirect("admin/gerenciarFilmes.xhtml");
        } else {
            // envia uma mensagem de erro
            messagesHelper.addMessage(new FacesMessage("Senha incorreta!"));
        }
        
    }
    
    
    
    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
}
