package org.cinema.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.cinema.entidades.jpa.ConfiguracaoDiaria;
import org.cinema.entidades.jpa.Filme;
import org.cinema.entidades.jpa.Horario;
import org.cinema.entidades.jpa.SessaoDeFilme;
import org.cinema.entidades.jpa.ConfiguracaoDiaria.DiaDaSemana;
import org.cinema.services.FilmeService;
import org.cinema.services.SessaoDeFilmeService;

@Named
@ViewScoped
public class CadastroDeSessaoBean implements Serializable {

    private static final long serialVersionUID = -899776579434552519L;

    private SessaoDeFilme sessao;
    private ConfiguracaoDiaria configuracaoDiaria;
    private Horario horario;

    @Inject
    private transient DetalhesDoFilmeBean detalhesDoFilmeBean;

    @Inject
    private transient SessaoDeFilmeService service;

    @Inject
    private transient FilmeService filmeService;

    @PostConstruct
    public void init() {
        limpar();
    }

    // o metodo do bean e executado em uma transacao, necessario nos beans gerenciados pelo CDI
    @Transactional
    public void salvar() {
        Filme filme = detalhesDoFilmeBean.getFilme(); // obtem o filme aberto na pagina de detalhes do filme
        sessao.setFilme(filme); // vincula a sessao com o filme
        filme.getSessoes().add(sessao); // adicionar a sessao na lista de sessoes do filme

        service.persist(sessao); // salva a sessao
    }
    
    // o metodo do bean e executado em uma transacao, necessario nos beans gerenciados pelo CDI
    @Transactional
    public void remover(Long id) {
        SessaoDeFilme sessao = service.buscar(id); // busca a sessao de filme
        service.remover(sessao); // remove a sessao de filme
    }

    // limpa o formulario de sessoes
    public void limpar() {
        sessao = new SessaoDeFilme();
        sessao.setConfiguracoesDiarias(new ArrayList<>());

        resetColetorDeConfiguracaoDiaria();
    }

    // reseta o coletor de horarios
    // coletor e um componente do primefaces
    public void resetColetorDeHorarios() {
        horario = new Horario();
        horario.setConfiguracaoDiaria(configuracaoDiaria);
    }
    
    // reseta o coletor de configucacoes diarias
    // coletor e um componente do primefaces
    public void resetColetorDeConfiguracaoDiaria() {
        configuracaoDiaria = new ConfiguracaoDiaria();
        configuracaoDiaria.setSessaoDeFilme(sessao);
        configuracaoDiaria.setHorarios(new ArrayList<>());
        resetColetorDeHorarios();
    }

    public SessaoDeFilme getSessao() {
        return sessao;
    }

    public void setSessao(SessaoDeFilme sessao) {
        this.sessao = sessao;
    }

    public List<SessaoDeFilme> getAll() {
        return service.list();
    }

    public ConfiguracaoDiaria getConfiguracaoDiaria() {
        return configuracaoDiaria;
    }

    public void setConfiguracaoDiaria(ConfiguracaoDiaria configuracaoDiaria) {
        this.configuracaoDiaria = configuracaoDiaria;
    }

    public Horario getHorario() {
        return horario;
    }

    public void setHorario(Horario horario) {
        this.horario = horario;
    }

    public List<SelectItem> diasDaSemana() {
        List<SelectItem> itens = new ArrayList<>();

        itens.add(new SelectItem("", ""));
        for (DiaDaSemana dia : DiaDaSemana.values()) {
            itens.add(new SelectItem(dia, dia.toString()));
        }

        return itens;
    }

}
