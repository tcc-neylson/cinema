package org.cinema.session;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;


// objeto de sessao que armazena se o usuario esta autenticado ou nao

@SessionScoped
public class SessaoDeAutenticacao implements Serializable{
    
    
    private boolean administradorEstaAutenticado;

    @PostConstruct
    public void init() {
        administradorEstaAutenticado = false;
    }

    public boolean isAdministradorEstaAutenticado() {
        return administradorEstaAutenticado;
    }

    public void setAdministradorEstaAutenticado(boolean administradorEstaAutenticado) {
        this.administradorEstaAutenticado = administradorEstaAutenticado;
    }
    
}
