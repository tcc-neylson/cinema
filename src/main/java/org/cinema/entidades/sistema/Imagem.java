package org.cinema.entidades.sistema;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.primefaces.model.UploadedFile;

// modelo que representa uma imagem
// os repositorio de imagens usam e retornam esse modelo

public class Imagem {

        // enumerado que representa o tipo da imagem
	public enum Tipo {
		PNG("image/png", "png"), JPEG("image/jpeg", "jpg");

		private String mimeType;
		private String formato;

		Tipo(String mimeType, String formato) {
			this.mimeType = mimeType;
			this.formato = formato;
		}

		public String getMimeType() {
			return mimeType;
		}

		public String getExtensao() {
			return "." + formato;
		}
		
		public String getFormato(){
			return formato;
		}
	}

	private InputStream input;
	private File file;
	private Tipo tipo;

	public Imagem(UploadedFile file) throws IOException {
		input = file.getInputstream();
		tipo = extrairTipoDaImagem(file);
	}
	
	public Imagem(File file, Tipo tipo){
		this.file = file;
		this.tipo = tipo;
	}

        // retorna o tipo da imagem
	private Tipo extrairTipoDaImagem(UploadedFile file) {
            
		switch (file.getContentType()) {
			case "image/png":
				return Tipo.PNG;
	
			case "image/jpg":
			case "image/jpeg":
				return Tipo.JPEG;
		}

		throw new IllegalArgumentException("arquivo com tipo inválido");
	}

        // retorna um input stream da imagem
	public InputStream getInputStream() throws FileNotFoundException {
		
		if(input == null && file != null){
			return new FileInputStream(file);
		}
			
		return input;
	}
	
	public Tipo getTipo() {
		return tipo;
	}

	public String getExtensaoDoArquivo() {
		return tipo.getExtensao();
	}
	
	public String getMimeType() {
		return tipo.getMimeType();
	}
	
	public File getFile() throws IllegalStateException {
		
		if(file == null)
			throw new IllegalStateException();
		
		return file;
	}

}
