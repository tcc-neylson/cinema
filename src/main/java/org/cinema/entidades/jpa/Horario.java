package org.cinema.entidades.jpa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Horario implements Serializable, PersistentEntity {

	private static final long serialVersionUID = 8272684164613232565L;


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@NotEmpty
	private String horario;
	
	@NotEmpty
	private String sala;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private ConfiguracaoDiaria configuracaoDiaria;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getHorario() {
		return horario;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}
	
	public ConfiguracaoDiaria getConfiguracaoDiaria() {
		return configuracaoDiaria;
	}
	public void setConfiguracaoDiaria(ConfiguracaoDiaria configuracaoDiaria) {
		this.configuracaoDiaria = configuracaoDiaria;
	}
	
	public String getSala() {
		return sala;
	}
	public void setSala(String sala) {
		this.sala = sala;
	}
	
	
	@Override
	public String toString() {
		return horario + " - " + sala;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Horario)) {
			return false;
		}
		Horario other = (Horario) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
}
