package org.cinema.entidades.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;


@Entity
public class SessaoDeFilme implements Serializable, PersistentEntity {

    private static final long serialVersionUID = 6752008655460538799L;

    public enum Video {
        _2D, _3D;

        public String toString() {
            return this == Video._2D ? "2D" : "3D";
        }

    };

    public enum Audio {
        DUBLADO, LEGENDADO, NACIONAL;

        public String toString() {
            switch (this) {
                case DUBLADO:
                    return "Dublado";
                case LEGENDADO:
                    return "Legendado";
                case NACIONAL:
                    return "Nacional";
            }

            return "";
        }
    };

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Selecione um tipo de vídeo")
    private Video video;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Seleciona um tipo de áudio")
    private Audio audio;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "sessaoDeFilme", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ConfiguracaoDiaria> configuracoesDiarias;

    @ManyToOne(fetch = FetchType.EAGER)
    private Filme filme;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public Audio getAudio() {
        return audio;
    }

    public void setAudio(Audio audio) {
        this.audio = audio;
    }

    public List<ConfiguracaoDiaria> getConfiguracoesDiarias() {
        return configuracoesDiarias;
    }

    public void setConfiguracoesDiarias(
            List<ConfiguracaoDiaria> configuracoesDiarias) {
        this.configuracoesDiarias = configuracoesDiarias;
    }

    public Filme getFilme() {
        return filme;
    }

    public void setFilme(Filme filme) {
        this.filme = filme;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (id != null) {
            result += "id: " + id;
        }

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SessaoDeFilme)) {
            return false;
        }
        SessaoDeFilme other = (SessaoDeFilme) obj;
        if (id != null) {
            if (!id.equals(other.id)) {
                return false;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
}
