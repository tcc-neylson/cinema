package org.cinema.entidades.jpa;

public interface PersistentEntity {

	Long getId();
	void setId(Long id);
}
