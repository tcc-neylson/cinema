package org.cinema.entidades.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Filme implements Serializable, PersistentEntity {

	private static final long serialVersionUID = -6844818934805046261L;
	
	public enum Censura {
		LIVRE("Livre"), _10ANOS("10 Anos"), _12ANOS("12 Anos"), 
		_14ANOS("14 Anos"), _16ANOS("16 Anos"), _18ANOS("18 Anos");
		
		private String nome;
		
		Censura(String nome) {
			this.nome = nome;
		}
		
		@Override
		public String toString() {
			return nome;
		}
	};
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	
	@NotEmpty
	private String titulo;
	
	@NotEmpty
	private String diretor;
	
	@Min(1990)
	@Max(2100)
	private int ano;
	
	@Min(1)
	private int duracaoEmMinutos;
	
	@NotEmpty
	@Column(columnDefinition="text")
	private String resumo;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private Censura censura;
	
	@ElementCollection(fetch=FetchType.EAGER)
	private List<String> generos;
	
	@OneToMany(mappedBy="filme", fetch=FetchType.EAGER, cascade=CascadeType.REMOVE, orphanRemoval=true)
	private List<SessaoDeFilme> sessoes;
	

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public String getDiretor() {
		return diretor;
	}

	public void setDiretor(String diretor) {
		this.diretor = diretor;
	}

	public int getDuracaoEmMinutos() {
		return duracaoEmMinutos;
	}

	public void setDuracaoEmMinutos(int duracaoEmMinutos) {
		this.duracaoEmMinutos = duracaoEmMinutos;
	}

	public String getResumo() {
		return resumo;
	}

	public void setResumo(String resumo) {
		this.resumo = resumo;
	}

	public Censura getCensura() {
		return censura;
	}

	public void setCensura(Censura censura) {
		this.censura = censura;
	}

	public List<String> getGeneros() {
		return generos;
	}
        
        public String getGenerosConcatenados() {
            return StringUtils.join(generos, ", ");
        }

	public void setGeneros(List<String> generos) {
		this.generos = generos;
	}

	public List<SessaoDeFilme> getSessoes() {
		return sessoes;
	}

	public void setSessoes(List<SessaoDeFilme> sessoes) {
		this.sessoes = sessoes;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (id != null)
			result += "id: " + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Filme)) {
			return false;
		}
		Filme other = (Filme) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
}