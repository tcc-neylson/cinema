package org.cinema.entidades.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;

@Entity
public class ConfiguracaoDiaria implements Serializable, PersistentEntity {

	private static final long serialVersionUID = -4968795464978603609L;

	public enum DiaDaSemana {
		SEGUNDA("Segunda-Feira"), TERCA("Terça-feira"), QUARTA("Quarta-feira"), QUINTA(
				"Quinta-feira"), SEXTA("Sexta-feira"), SABADO("Sábado"), DOMINGO(
				"Domingo"), DIARIAMENTE("Diariamente");
		
		private String string;
		
		private DiaDaSemana(String string) {
			this.string = string;
		}
		
		public String toString() {
			return string;
		}
	};

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToMany(mappedBy = "configuracaoDiaria", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Horario> horarios;

	@NotNull
	@Enumerated(EnumType.STRING)
	private DiaDaSemana diaDaSemana;


	@ManyToOne(fetch = FetchType.EAGER)
	private SessaoDeFilme sessaoDeFilme;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Horario> getHorarios() {
		return horarios;
	}
        
        public String getHorariosConcatenados() {
            List<String> horariosESalas = new ArrayList<>();
            
            for(Horario horario : horarios) {
                horariosESalas.add(horario.getHorario() + "h - " + horario.getSala());
            }
            
            return StringUtils.join(horariosESalas, ", ");
        }

	public void setHorarios(List<Horario> horarios) {
		this.horarios = horarios;
	}

	public DiaDaSemana getDiaDaSemana() {
		return diaDaSemana;
	}

	public void setDiaDaSemana(DiaDaSemana diaDaSemana) {
		this.diaDaSemana = diaDaSemana;
	}

	public SessaoDeFilme getSessaoDeFilme() {
		return sessaoDeFilme;
	}

	public void setSessaoDeFilme(SessaoDeFilme sessaoDeFilme) {
		this.sessaoDeFilme = sessaoDeFilme;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append(diaDaSemana.toString());

		builder.append(" (" + StringUtils.join(horarios, " | ") + ")");

		return builder.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ConfiguracaoDiaria)) {
			return false;
		}
		ConfiguracaoDiaria other = (ConfiguracaoDiaria) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}

		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

}
