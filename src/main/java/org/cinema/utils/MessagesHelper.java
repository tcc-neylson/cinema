package org.cinema.utils;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@ApplicationScoped
public class MessagesHelper {
    
    // injeta o objeto FacesContext do jsf
    @Inject
    private FacesContext facesContext;
    
    // envia uma mensagem pelo jsf
    public void addMessage(FacesMessage facesMessage) {
        facesContext.addMessage(null, facesMessage);
    }
}
