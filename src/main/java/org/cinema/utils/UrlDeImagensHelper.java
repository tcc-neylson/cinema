package org.cinema.utils;

import javax.enterprise.context.ApplicationScoped;

// classe para auxiliar na criaçao de enderecos de imagens
// esses enderecos apontam para a servlet de imagens


@ApplicationScoped
public class UrlDeImagensHelper {

    
    
    public String getUrlDaImagemDoFilme(Long filmeId, String nome) {
        return "/imagens?recurso=filmes&id=" + filmeId + "&nome=" + nome;
    }
    
    
    
    public String getUrlDeImagemTemporaria(String id) {
        return "/imagens?recurso=temp&id=" + id;
    }
}
