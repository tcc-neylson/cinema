package org.cinema.filter;

import java.io.IOException;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.cinema.session.SessaoDeAutenticacao;


// filtro de autenticaçao 
// se o usuario nao estiver autenticado e o usuario tentar acessar alguma pagina
// da pasta admin ele sera redirecionado para a pagina autenticar.xhtml

@WebFilter("/admin/*")
public class AutenticacaoFilter implements Filter {

    // injeta o objeto de sessao
    @Inject
    private SessaoDeAutenticacao sessao;
    
    @Override
    public void init(FilterConfig fc) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        
        if(sessao.isAdministradorEstaAutenticado()) {
            chain.doFilter(request, response); // permite que a requisiçao continue
        } else {
            response.sendRedirect(request.getContextPath() + "/autenticar.xhtml"); // redireciona o usuario para a pagina de autenticacao
        }
        
    }

    @Override
    public void destroy() {
    }
    
    
    
    
    
}
