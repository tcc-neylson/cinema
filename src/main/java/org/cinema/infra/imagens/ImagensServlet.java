package org.cinema.infra.imagens;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.cinema.entidades.sistema.Imagem;

/**
 * Sevlet para servir imagens, esta configurada no arquivo web.xml
 */
public class ImagensServlet extends HttpServlet {
    

    // injeta o repositorio de imagens temporarias
    @Inject
    private RepositorioDeImagensTemporarias imagensTemporarias;

    // injeta o repositorio de imagens de filmes
    @Inject
    private RepositorioDeImagensDeFilmes repoDeImagensDeFilmes;

    
    public ImagensServlet() {
        super();
    }

    
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        // obtem o atributo "id" da querystring
        // pode ser o id da imagem temporaria ou o id do filme
        String id = request.getParameter("id") + "";

        // obtem o atributo recurso da querystring
        // o recurso pode ser "filmes" ou "temp"
        String recurso = request.getParameter("recurso") + "";

        Imagem imagem = null;
        
        switch (recurso) {
            case "temp":
                try {
                    // obtem a imagem do repositorio
                    imagem = imagensTemporarias.obterImagemCom(UUID.fromString(id));
                } catch (FileNotFoundException | IllegalArgumentException e) {
                    response.setStatus(404); // se ocorrer um erro retorna uma resposta http not found
                    return;
                }

                break;
            case "filmes":
                // se o recurso for "filmes" obtem outro parametro com o nome da imagem da querystring
                String nome = request.getParameter("nome");
                
                try {
                    // obtem a imagem do repositorio
                    imagem = repoDeImagensDeFilmes.obterImagemCom(Long.valueOf(id), nome);
                } catch (FileNotFoundException | NumberFormatException e) {
                    response.setStatus(404); // se ocorrer um erro retorna uma resposta http not found
                    return;
                }
                break;

            default:
                response.setStatus(404);
                return;

        }
        
        
        InputStream stream = imagem.getInputStream(); // obtem o stream da imagem
        response.setContentType(imagem.getMimeType()); // define o mime-type da imagem como o tipo de conteudo de resposta da requisiçao http
        OutputStream out = response.getOutputStream(); // o obtem o stream da resposta http

        if (stream != null) {
            IOUtils.copy(stream, out); // copia o stream da imagem no stream da resposta http
            stream.close();
        }

        out.close();
    }

    // nao faz nada
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
    }

}
