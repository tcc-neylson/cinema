package org.cinema.infra.imagens;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.commons.io.FileUtils;

// limpa arquivos da pasta de arquivos temporarios

@Singleton // escopo singleton de ejb
@Startup // inicializa o ejb na inicializaçao
public class LimpezaDeArquivosTemporarios {
    
    
    // diretorio de arquivos temporarios
    private String diretorioTemporario = FileUtils.getUserDirectoryPath() + "/cinema/uploads/temp";
    
    // metodo executado logo apos o CDI construir o objeto
    @PostConstruct
    public void init() {
        try {
            // cria o diretorio de arquivos temporarios
            Files.createDirectories(Paths.get(diretorioTemporario));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Erro ao criar o diretório de arquivos temporários");
        }
    }

    // essa anotaçao executa o metodo a cada 1 minuto
    @Schedule(minute = "*/1", hour = "*", persistent = false)
    public void run() throws IOException {
        
        // obtem todos os arquivos do diretorio temporario
        Collection<File> files = FileUtils.listFiles(new File(diretorioTemporario), null, false);
        
        // obtem o momento atual em milisegundos e subtrai 5 minutos
        long time = Calendar.getInstance().getTimeInMillis();
        time -= 300000;
        
        // para cada arquivo
        for (File file : files) {
            
            // se o arquivo for mais velho que 5 minutos entao remove ele
            if (!FileUtils.isFileNewer(file, time)) {
                file.delete();
            }
        }
        
        System.out.println("Limpeza de arquivos temporários realizada!");
    }
}
