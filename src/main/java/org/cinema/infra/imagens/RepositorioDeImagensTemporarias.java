package org.cinema.infra.imagens;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;

import org.cinema.entidades.sistema.Imagem;
import org.cinema.entidades.sistema.Imagem.Tipo;

// repositorio de imagens temporarias

@ApplicationScoped
public class RepositorioDeImagensTemporarias extends RepositorioDeImagens {

        // armazena uma imagem temporaria
	
	public UUID salvar(Imagem imagem) throws IOException {
		UUID id = UUID.randomUUID();
		Tipo tipo = imagem.getTipo();

		String nomeDoArquivo = construirNomeDoArquivo(id.toString(), tipo);

		storage.salvarArquivo(imagem.getInputStream(), "temp", nomeDoArquivo);

		return id;
	}
        
        // busca uma imagem temporaria

	public Imagem obterImagemCom(UUID id) throws FileNotFoundException {

		Tipo tipo = descobrirTipoDaImagem("temp", id.toString());
		File file = storage.obterArquivo("temp", construirNomeDoArquivo(id.toString(), tipo));
		
		return new Imagem(file, tipo);
	}
        
        // remove uma imagem temporaria

	public void removerImagemCom(UUID id) {
		Tipo tipo;
		try {
			tipo = descobrirTipoDaImagem("temp", id.toString());
			storage.removerArquivo("temp", construirNomeDoArquivo(id.toString(), tipo));
		} catch (FileNotFoundException e) {
		}
	}

	
}
