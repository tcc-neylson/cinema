package org.cinema.infra.imagens;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.enterprise.context.ApplicationScoped;
import javax.imageio.ImageIO;

import org.cinema.entidades.sistema.Imagem;
import org.cinema.entidades.sistema.Imagem.Tipo;

// repositorio de imagens de filmes
// tem a funcao de salvar, remover e buscar imagens de filmes

// utiliza a classe FileStorage para realizar as operaçoes

@ApplicationScoped
public class RepositorioDeImagensDeFilmes extends RepositorioDeImagens {

    // salva a imagem do filme
    public void salvar(Imagem imagem, Long filmeId, String nome) throws IOException {

        // armazena a imagem
        File file = storage.salvarArquivo(imagem.getInputStream(), "filmes/" + filmeId,
                construirNomeDoArquivo(nome, imagem.getTipo()));

        // esse trecho dentro da thread redimensiona a imagem e substitui o conteudo da imagem armazenada
        // Veja mais em: http://www.mkyong.com/java/how-to-resize-an-image-in-java/
        Thread thread = new Thread(() -> {

            BufferedImage original = null;
            try {
                original = ImageIO.read(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
            int type = original.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : original.getType();
            BufferedImage newImage = new BufferedImage(250, 350, type);
            Graphics2D g = newImage.createGraphics();
            g.drawImage(original, 0, 0, 250, 350, null);
            g.dispose();
            try {
                ImageIO.write(newImage, imagem.getTipo().getFormato(), file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }

    // remove a imagem do filme
    public void removerImagem(Long filmeId, String nome) throws FileNotFoundException {
        Tipo tipo = descobrirTipoDaImagem("filmes/" + filmeId, nome);

        storage.removerArquivo("filmes/" + filmeId, construirNomeDoArquivo(nome, tipo));
    }

    // busca a imagem do filme
    public Imagem obterImagemCom(Long filmeId, String nome) throws FileNotFoundException {
        Tipo tipo = descobrirTipoDaImagem("filmes/" + filmeId, nome);

        File file = storage.obterArquivo("filmes/" + filmeId, construirNomeDoArquivo(nome, tipo));

        return new Imagem(file, tipo);
    }

}
