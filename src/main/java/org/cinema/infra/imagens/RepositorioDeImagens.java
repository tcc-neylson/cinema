package org.cinema.infra.imagens;

import java.io.FileNotFoundException;

import javax.inject.Inject;

import org.cinema.entidades.sistema.Imagem;
import org.cinema.entidades.sistema.Imagem.Tipo;
import org.cinema.infra.storage.FileStorage;

// classe abstrata dos repositorios de imagens

public abstract class RepositorioDeImagens {
    
    // injeta um objeto da interface FileStorage, o cdi ira buscar pela implementacao da interface
    @Inject
    protected FileStorage storage;
    
    
    // descobre se a imagem armazenada no diretorio e do tipo png ou jpeg
    protected Tipo descobrirTipoDaImagem(String pasta, String nome) throws FileNotFoundException {

        String nomeDaImagemJpeg = construirNomeDoArquivo(nome, Imagem.Tipo.JPEG);
        String nomeDaImagemPng = construirNomeDoArquivo(nome, Imagem.Tipo.PNG);

        // tenta verificar se existe uma imagem na pasta com o nome e a extensao jpeg
        if (storage.arquivoExiste(pasta, nomeDaImagemJpeg)) {
            return Imagem.Tipo.JPEG;
        }

        // tenta verificar se existe uma imagem na pasta com o nome e a extensao png
        if (storage.arquivoExiste(pasta, nomeDaImagemPng)) {
            return Imagem.Tipo.PNG;
        }

        throw new FileNotFoundException();
    }

    // concatena o nome com o tipo
    protected String construirNomeDoArquivo(String nome, Tipo tipo) {
        return nome + tipo.getExtensao();
    }

}
