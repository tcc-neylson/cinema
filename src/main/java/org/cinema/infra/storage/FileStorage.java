package org.cinema.infra.storage;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public interface FileStorage {

	public File salvarArquivo(InputStream inputStream, String pasta, String nomeDoArquivo) throws IOException;
	public boolean arquivoExiste(String pasta, String nomeDoArquivo);
	public File obterArquivo(String pasta, String nomeDoArquivo);
	public void removerArquivo(String pasta, String nomeDoArquivo);
}
