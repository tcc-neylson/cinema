package org.cinema.infra.storage;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.io.FileUtils;

// implementacao da interface FileStorage
@ApplicationScoped
public class LocalFileStorage implements FileStorage {
    
    // diretorio de armazenamento de arquivos
    // diretorio do usuario concatenado com /cinema/uploads
    private String rootPath = FileUtils.getUserDirectoryPath() + "/cinema/uploads";

    // salva o input stream na pasta com o nome
    public File salvarArquivo(InputStream inputStream, String pasta,
            String nomeDoArquivo) throws IOException {

        File file = Paths.get(rootPath, pasta, nomeDoArquivo).toFile();
        FileUtils.copyInputStreamToFile(inputStream, file);

        return file;
    }

    // verifica se o arquivo existe
    public boolean arquivoExiste(String pasta, String nomeDoArquivo) {
        File file = Paths.get(rootPath, pasta, nomeDoArquivo).toFile();

        return file.exists();
    }

    //busca o arquivo
    public File obterArquivo(String pasta, String nomeDoArquivo) {
        return Paths.get(rootPath, pasta, nomeDoArquivo).toFile();
    }

    // remove o arquivo
    public void removerArquivo(String pasta, String nomeDoArquivo) {
        File file = Paths.get(rootPath, pasta, nomeDoArquivo).toFile();

        FileUtils.deleteQuietly(file);
    }
}
