package org.cinema.services;

import javax.ejb.Stateless;
import org.cinema.entidades.jpa.Filme;

@Stateless
public class FilmeService extends EntityService<Filme>{

	public FilmeService() {
		super(Filme.class);
	}

}
