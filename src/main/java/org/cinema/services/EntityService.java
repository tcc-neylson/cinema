package org.cinema.services;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import org.cinema.entidades.jpa.PersistentEntity;

public abstract class EntityService<T extends PersistentEntity> {

	@PersistenceContext
	protected EntityManager entityManager;
	
	private Class<T> entityClass;
	
	public EntityService(Class<T> entityClass) {
		this.entityClass = entityClass;
	}
	
	public void persist(T entity) {
		entityManager.persist(entity);
	}
	
	public T buscar(Long id) {
		return (T) entityManager.find(entityClass, id);
	}

	public void remover(T entity) {
		entityManager.remove(entity);		
	}

	public T merge(T entity) {
		return entityManager.merge(entity);
	}
	
	public void refresh(T entity) {
		entityManager.refresh(entity);
	}
	
	public List<T> list() {
		CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		
		return entityManager.createQuery(cq).getResultList();
	}
}
