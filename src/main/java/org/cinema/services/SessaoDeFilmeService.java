package org.cinema.services;

import javax.ejb.Stateless;
import org.cinema.entidades.jpa.SessaoDeFilme;

@Stateless
public class SessaoDeFilmeService extends EntityService<SessaoDeFilme>{

	public SessaoDeFilmeService() {
		super(SessaoDeFilme.class);
	}

}
